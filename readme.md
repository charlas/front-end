# Charla consideraciones Front End
##### En este documento se encuentra información referente a la charla del 16 de octubre de 2014.

## Diapositivas

https://docs.google.com/a/pragma.com.co/presentation/d/1y3lp7Ed86nHEiMFr5ToKFphRTAbDAvMMcxA8qse42ag/edit?usp=sharing

## Referencias
* [Novedades HTML5](http://www.w3schools.com/html/html5_new_elements.asp)
* [Sobre el DOCTYPE](http://www.w3schools.com/tags/tag_doctype.asp)
* [Elementos según DOCTYPE](http://www.w3schools.com/tags/ref_html_dtd.asp)
* [CSS específicos](http://www.sitepoint.com/get-specific-css-styles/)
* [DOM reference](http://www.w3schools.com/jsref/)
* [CSS reference](http://www.w3schools.com/cssref/css_selectors.asp)
* [Pseudo elementos y pseudo clases](http://www.w3schools.com/css/css_pseudo_elements.asp)